# Project_10

C'est une application permettant de remonter et suivre des problèmes techniques (issue tracking system). Cette solution s’adresse à des entreprises clientes, en B2B. 

## Pré-requis

* python3
* Poetry


## Installation
- Installation Poetry
Pour commencer, il faudra installer Poetry


```
$ pip install --user poetry

Après l'installation, se deplacer dans le dossier courant "project_09" et faire:

$ poetry install 

En faisant cette commande, cela installe toute la dépendance du projet

l'étape suivante c'est d'activer le virutalenv de Poetry tout en restant dans le dossier courant:

$ poetry shell

```

## Lien Documentation Poetry

- [Lien document offciel poetry](https://python-poetry.org/docs/cli/)


## Démarrage
```
$ python3 manage.py migrate

$ python3 manage.py runserver

```

## Documenation API

Les liens de chaque "endpoint"


- [Login](https://documenter.getpostman.com/view/8704915/UVJeFG7T)

- [Project ](https://documenter.getpostman.com/view/8704915/UVJeFG3A )

- [Contributor](https://documenter.getpostman.com/view/8704915/UVJeFG7V)

- [Issue](https://documenter.getpostman.com/view/8704915/UVJeFG7W)

- [Comment](https://documenter.getpostman.com/view/8704915/UVJeFG7Y)
