from django.contrib import admin
from django.db import router
from django.urls import path, include
from rest_framework import routers

from myapis.views import RegisterView, LogoutView, MyObtainTokenPairView

from rest_framework_simplejwt.views import TokenRefreshView

from myapis.views import ProjectAPIViewset, ContributorAPIViewset, IssueAPIViewset, CommentAPIViewset

project_list = ProjectAPIViewset.as_view({'get': 'list', 'post': 'create'})
project_detail = ProjectAPIViewset.as_view({
	'get': 'retrieve',
	'put': 'update',
	'delete': 'destroy'
})
contributor_list = ContributorAPIViewset.as_view({'post': 'create', 'delete': 'destroy'})
contributor_detail = ContributorAPIViewset.as_view({'get': 'retrieve'})
issue_list = IssueAPIViewset.as_view({'get': 'list', 'post': 'create'})
issue_detail = IssueAPIViewset.as_view({'put': 'update', 'delete': 'destroy'})
comment_list = CommentAPIViewset.as_view({'get': 'list', 'post': 'create'})
comment_detail = CommentAPIViewset.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})

urlpatterns = [
	path('project/', project_list, name='project'),
	path('project/<int:project_id>/', project_detail, name='project-detail'),
	path('project/<int:project_id>/users/<int:id_user>/', contributor_list, name='add-contributor'),
	path('project/<int:project_id>/users/', contributor_detail, name='project-contributors'),
	path('project/<int:project_id>/issues/', issue_list, name='issue-list'),
	path('project/<int:project_id>/issues/<int:id_issue>/', issue_detail, name='issue-project'),
	path('project/<int:project_id>/issues/<int:id_issue>/comments/', comment_list, name='comment-issue'),
	path('project/<int:project_id>/issues/<int:id_issue>/comments/<int:id_com>', comment_detail, name='comment-issue-detail'),
	path('login/', MyObtainTokenPairView.as_view(), name='token_obtain_pair'),
	path('login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
	path('register/', RegisterView.as_view(), name='auth_register'),
	path('logout/', LogoutView.as_view(), name='auth_logout')
]


