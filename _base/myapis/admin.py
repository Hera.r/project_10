from django.contrib import admin
from myapis.models import Project, Contributor, Issue, Comment


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    fields = ('title', 'description', 'type_project', 'author')
    list_display = ('title', 'description', 'type_project', 'author')


@admin.register(Contributor)
class ContributorAdmin(admin.ModelAdmin):
    fields = ('project', 'user', 'role')
    list_display = ('project', 'user', 'role')


@admin.register(Issue)
class IssueAdmin(admin.ModelAdmin):
    fields = ('title', 'desc', 'tag', 'priority', 'project', 'status', 'author', 'assignee')
    list_display = ('title', 'desc', 'tag', 'priority', 'project', 'status', 'author', 'assignee')


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    fields = ('description', 'author', 'issue')
    list_display = ('description', 'author', 'issue')
