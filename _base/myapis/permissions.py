from rest_framework import permissions
from .models import Project, Contributor, Issue, Comment
from rest_framework import status


class AuthorOrContributorProject(permissions.BasePermission):
	message = 'access denied for non-authors or contributors'

	def has_permission(self, request, view):
		project_user = None
		contributor_user = None

		id_project = request.parser_context.get('kwargs', None).get('project_id', None)
		project = Project.objects.filter(id=id_project, author=request.user).first()
		contributor = Contributor.objects.filter(user=request.user, project=id_project).first()
		if project:
			project_user = project.author
		if contributor:
			contributor_user = contributor.user

		if request.user.is_authenticated:
			if view.action in ['list', 'create']:
				return request.user

			elif view.action in ['retrieve']:
				return request.user == project_user or request.user == contributor_user

			elif view.action in ['update', 'destroy']:
				return request.user == project_user


class AuthorOrContributor(permissions.BasePermission):
	message = 'access denied for non-authors or contributors'

	def has_permission(self, request, view):
		project_user = None
		contributor_user = None

		id_project = request.parser_context.get('kwargs', None).get('project_id', None)
		project = Project.objects.filter(id=id_project, author=request.user).first()
		contributor = Contributor.objects.filter(user=request.user, project=id_project).first()

		if project:
			project_user = project.author
		if contributor:
			contributor_user = contributor.user

		if request.user.is_authenticated:
			if view.action == 'retrieve':
				return request.user == project_user or request.user == contributor_user
			elif view.action in ['create', 'destroy']:
				return request.user == project_user


class AuthorOrContributorIssue(permissions.BasePermission):
	message = 'access denied for non-authors or contributors'

	def has_permission(self, request, view):
		project_user = None
		contributor_user = None
		issue_user = None

		id_project = request.parser_context.get('kwargs', None).get('project_id', None)
		project = Project.objects.filter(id=id_project, author=request.user).first()
		contributor = Contributor.objects.filter(user=request.user, project=id_project).first()
		issue = Issue.objects.filter(author=request.user, project=id_project).first()
		if project:
			project_user = project.author
		if contributor:
			contributor_user = contributor.user
		if issue:
			issue_user = issue.author
		if request.user.is_authenticated:
			if view.action in ['list', 'create']:
				return request.user == project_user or request.user == contributor_user
			elif view.action in ['update', 'destroy']:
				return request.user == issue_user


class AuthorOrContributorComment(permissions.BasePermission):
	message = 'access denied for non-authors or contributors'

	def has_permission(self, request, view):
		project_user = None
		contributor_user = None
		comment_user = None

		id_project = request.parser_context.get('kwargs', None).get('project_id', None)
		id_issue = request.parser_context.get('kwargs', None).get('id_issue', None)
		project = Project.objects.filter(id=id_project, author=request.user).first()
		contributor = Contributor.objects.filter(user=request.user, project=id_project).first()
		comment = Comment.objects.filter(author=request.user, issue=id_issue).first()

		if project:
			project_user = project.author
		if contributor:
			contributor_user = contributor.user
		if comment:
			comment_user = comment.author

		if request.user.is_authenticated:
			if view.action in ['list', 'create', 'retrieve']:
				return request.user == project_user or request.user == contributor_user
			elif view.action in ['update', 'destroy']:
				return request.user == comment_user
