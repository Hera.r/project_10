from django.db.models import fields
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import Project, Contributor, Issue, Comment
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework.validators import UniqueValidator
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.models import User


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
	@classmethod
	def get_token(cls, user):
		token = super(MyTokenObtainPairSerializer, cls).get_token(user)

		# Add custom claims
		token['username'] = user.username
		return token


class RegisterSerializer(serializers.ModelSerializer):
	password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
	password2 = serializers.CharField(write_only=True, required=True)

	class Meta:
		model = User
		fields = ("username", "password", "password2")
		extra_kwargs = {
			'username': {'required': True},
		}

	def validate(self, attrs):
		if attrs['password'] != attrs['password2']:
			raise serializers.ValidationError({"password": "Password fields didn't match."})

		return attrs

	def create(self, validated_data):
		user = User.objects.create(
			username=validated_data['username'],
		)

		user.set_password(validated_data['password'])
		user.save()

		return user


class ProjectSerializer(ModelSerializer):
	class Meta:
		model = Project
		fields = ['id', 'title', 'description', 'type_project']


class ContributorSerializer(ModelSerializer):
	class Meta:
		model = Contributor
		fields = ['id', 'project', 'role']


class IssueSerializer(ModelSerializer):
	class Meta:
		model = Issue
		fields = ['id', 'title', 'desc', 'tag', 'priority', 'project', 'status', 'author', 'assignee', 'created_time']


class CommentSerializer(ModelSerializer):
	class Meta:
		model = Comment
		fields = ['id', 'description', 'author', 'issue', 'created_time']
