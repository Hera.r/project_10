from django.db import models
from django.conf import settings


PROJECT_CHOICES = (('Back-end', 'Back-end'), ('Front-end', 'Front-end'), ('iOS', 'iOS',), ('Android', 'Android'))
PRIORITY_CHOICES = (('Faible', 'Faible'), ('Moyenne', 'Moyenne'), ('Elevé', 'Elevé'))
STATUS_CHOICES = (('A faire', 'A faire'), ('En cours', 'En cours'), ('Terminé', 'Terminé'))
# faire choice de role 


class Project(models.Model):
	title = models.CharField(max_length=100)
	description = models.CharField(max_length=200)
	type_project = models.CharField(choices=PROJECT_CHOICES, default='Front-end', max_length=100)
	author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="auteur")

	def __str__(self):
		return self.title


class Contributor(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	project = models.ForeignKey(Project, on_delete=models.CASCADE)
	# permission = models.CharField(choices= , default='Permissions', max_length=100)
	role = models.CharField(max_length=50) 

	def __str__(self):
		return self.project.title


class Issue(models.Model):
	title = models.CharField(max_length=100)
	desc = models.CharField(max_length=200)
	tag = models.CharField(max_length=50)
	priority = models.CharField(choices=PRIORITY_CHOICES, default='Front-end', max_length=100)
	project = models.ForeignKey(Project, on_delete=models.CASCADE)
	status = models.CharField(choices=STATUS_CHOICES, default='Front-end', max_length=100)
	author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="personne")
	assignee = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	created_time = models.DateTimeField(auto_now_add=True, null=True)

	def __str__(self):
		return self.title


class Comment(models.Model):
	description = models.CharField(max_length=100)
	author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="utilisateur")
	issue = models.ForeignKey(Issue, on_delete=models.CASCADE)
	created_time = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.description

