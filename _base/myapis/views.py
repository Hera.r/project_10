from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import exceptions
from rest_framework import status

from .models import Project, Contributor, Issue, Comment
from .serializers import ProjectSerializer, MyTokenObtainPairSerializer, ContributorSerializer, IssueSerializer, CommentSerializer
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.permissions import AllowAny
from django.contrib.auth.models import User
from .serializers import RegisterSerializer
from rest_framework import generics
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import get_object_or_404
from .permissions import AuthorOrContributorProject, AuthorOrContributor, AuthorOrContributorIssue, AuthorOrContributorComment


class MyObtainTokenPairView(TokenObtainPairView):
	permission_classes = (AllowAny,)
	serializer_class = MyTokenObtainPairSerializer


class RegisterView(generics.CreateAPIView):
	queryset = User.objects.all()
	permission_classes = (AllowAny,)
	serializer_class = RegisterSerializer


class LogoutView(APIView):
	permission_classes = (IsAuthenticated,)

	def post(self, request):
		print(request.data["refresh"])
		try:
			refresh_token = request.data["refresh_token"]
			token = RefreshToken(refresh_token)
			token.blacklist()
			return Response(status=status.HTTP_205_RESET_CONTENT)
		except Exception as e:
			return Response(status=status.HTTP_400_BAD_REQUEST)


class ProjectAPIViewset(viewsets.ViewSet):
	permission_classes = [AuthorOrContributorProject]

	def list(self, request):
		queryset = Project.objects.all()
		serializer = ProjectSerializer(queryset, many=True)
		return Response(serializer.data)

	def retrieve(self, request, project_id=None):
		queryset = Project.objects.all()
		details = get_object_or_404(queryset, pk=project_id)
		serializer = ProjectSerializer(details)
		return Response(serializer.data)

	def create(self, request):
		serializer = ProjectSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save(author=request.user)

		return Response(serializer.data)

	def update(self, request, project_id=None):
		project = Project.objects.get(pk=project_id)
		serializer = ProjectSerializer(instance=project, data=request.data)

		if serializer.is_valid():
			serializer.save()

		return Response(serializer.data)

	def destroy(self, request, project_id=None):
		project = Project.objects.get(id=project_id)
		project.delete()

		return Response('delete succes! ')


class ContributorAPIViewset(viewsets.ViewSet):
	permission_classes = [AuthorOrContributor]

	def retrieve(self, request, project_id=None):
		details = Contributor.objects.filter(project=project_id)
		serializer = ContributorSerializer(details, many=True)

		return Response(serializer.data)

	def create(self, request, *args, **kwargs):
		user = User.objects.get(id=kwargs['id_user'])
		request.data['project'] = kwargs['project_id']
		request.data['role'] = "contributor"
		serializer = ContributorSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save(user=user)

		return Response(serializer.data)

	def destroy(self, *args, **kwargs):
		project = Contributor.objects.filter(project=kwargs['project_id'], user=kwargs['id_user'])
		project.delete()

		return Response('delete succes!')


class IssueAPIViewset(viewsets.ViewSet):
	permission_classes = [AuthorOrContributorIssue]

	def list(self, request, *args, **kwargs):
		details = Issue.objects.filter(project=kwargs['project_id'])
		serializer = IssueSerializer(details, many=True)

		return Response(serializer.data)

	def create(self, request, *args, **kwargs):
		project = Project.objects.get(id=kwargs['project_id'])
		request.data['project'] = project.id
		request.data['author'] = request.user.id
		request.data['assignee'] = request.user.id
		serializer = IssueSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
		else:
			print(serializer.errors)
		return Response(serializer.data)

	def update(self, request, *args, **kwargs):
		issue = Issue.objects.filter(project=kwargs['project_id'], id=kwargs['id_issue']).first()
		request.data['project'] = issue.project.id
		request.data['author'] = request.user.id
		request.data['assignee'] = request.user.id
		serializer = IssueSerializer(issue, data=request.data)
		if serializer.is_valid():
			serializer.save()
		else:
			print(serializer.errors)

		return Response(serializer.data)

	def destroy(self, *args, **kwargs):
		issue = Issue.objects.filter(project=kwargs['project_id'], id=kwargs['id_issue'])
		issue.delete()
		return Response('delete succes!')


class CommentAPIViewset(viewsets.ViewSet):
	permission_classes = [AuthorOrContributorComment]

	def list(self, request, *args, **kwargs):
		issue = Issue.objects.filter(project=kwargs['project_id'], id=kwargs['id_issue']).first()
		details = Comment.objects.filter(issue=issue)
		serializer = CommentSerializer(details, many=True)
		return Response(serializer.data)

	def retrieve(self, request, *args, **kwargs):
		issue = Issue.objects.filter(project=kwargs['project_id'], id=kwargs['id_issue']).first()
		comment = Comment.objects.filter(issue=issue, id=kwargs['id_com'])
		serializer = CommentSerializer(comment, many=True)
		return Response(serializer.data)

	def create(self, request, *args, **kwargs):
		issue = Issue.objects.filter(project=kwargs['project_id'], id=kwargs['id_issue']).first()
		request.data['author'] = request.user.id
		request.data['issue'] = issue.id
		serializer = CommentSerializer(data=request.data)
		if serializer.is_valid():
			serializer.save()
		else:
			print(serializer.errors)
		return Response(serializer.data)

	def update(self, request, *args, **kwargs):
		issue = Issue.objects.filter(project=kwargs['project_id'], id=kwargs['id_issue']).first()
		details = Comment.objects.filter(issue=issue, id=kwargs['id_com']).first()
		request.data['author'] = request.user.id
		request.data['issue'] = issue.id
		serializer = CommentSerializer(details, data=request.data)
		if serializer.is_valid():
			serializer.save()
		else:
			print(serializer.errors)

		return Response(serializer.data)

	def destroy(self, *args, **kwargs):
		issue = Issue.objects.filter(project=kwargs['project_id'], id=kwargs['id_issue']).first()
		comment = Comment.objects.filter(issue=issue, id=kwargs['id_com'])
		comment.delete()
		return Response('delete succes!')
